#!/bin/bash

cmsswdir=/afs/cern.ch/work/m/mwadud/private/CMSSW_9_4_0_patch1/src/
workdir=/afs/cern.ch/work/m/mwadud/private/ECAL/Sep25/
jobdir=/afs/cern.ch/work/m/mwadud/private/ECAL/Sep25/jobs/5000/

source /afs/cern.ch/work/m/mwadud/private/bin/cmsset_default.sh;
cd ${cmsswdir}; eval `scramv1 runtime -sh`; cd -;
cd ${workdir}