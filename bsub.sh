#!/bin/bash

cmsswdir=/afs/cern.ch/work/m/mwadud/private/CMSSW_9_4_0_patch1/src/
workdir=/afs/cern.ch/work/m/mwadud/private/ECAL/Oct11/
jobdir=/afs/cern.ch/work/m/mwadud/private/ECAL/Oct11/jobs/5000/

source /afs/cern.ch/work/m/mwadud/private/bin/cmsset_default.sh;
cd ${cmsswdir}; eval `scramv1 runtime -sh`; cd -;
cd ${workdir}


inputfile=${jobdir}"prompt_reco_2017.root"
outputdir=${jobdir}"graphs_prompt_reco_2017/"


scripitfile=${workdir}"submit_job.sh"
macro=${workdir}"submit_macro.C"
submitfile=${workdir}"condor_sub.sh"

mkdir -p $outputdir
mkdir -p ${outputdir}log/

for i in {0..11}
# for i in 3
do
#i=0		
		tempfile=$outputdir"submit_macro_$i.C"
		cp $macro $tempfile
		sed -i 's|graphID|'$i'|g' $tempfile
		sed -i 's|inputfile|'$inputfile'|g' $tempfile
		sed -i 's|outputdir|'$outputdir'|g' $tempfile
		sed -i 's|#workdir|'$workdir'|g' $tempfile

		tempfile2=$outputdir"submit_job_$i.sh"
		cp $scripitfile $tempfile2
		sed -i 's|macrofile|'$tempfile'|g' $tempfile2
		sed -i 's|#cmsswdir|'${cmsswdir}'|g' $tempfile2
		sed -i 's|#jobdir|'${outputdir}'|g' $tempfile2

		# tempfile3=$outputdir"condor_submit_$i.sh"
		# cp ${submitfile} ${tempfile3}
		# sed -i 's|#scriptfile|'$tempfile2'|g' $tempfile3
		# sed -i 's|#jobdir|'$outputdir'|g' $tempfile3
		# sed -i 's|graphID|'$i'|g' $tempfile3

		# condor_submit ${tempfile3} &

		# echo ${tempfile2}

		bsub -R "pool>20" -q 2nd -o ${outputdir}log/ -J ${tempfile} < ${tempfile2} &

		# bsub -R "pool>50" -q 2nd -o /afs/cern.ch/work/m/mwadud/private/ECAL/Aug2/log/ < $tempfile2
		#cat $tempfile2
		#printf "\n \n \n"
		#rm $tempfile
done
