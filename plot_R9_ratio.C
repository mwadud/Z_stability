#include "plot_tools.cxx"


std::string jobdir = "/afs/cern.ch/work/m/mwadud/private/ECAL/Sep25/jobs/5000/";

std::vector<std::string> files = {"Cal_Oct2017F_ref.root", "Cal_Oct2017E_ref.root",
                                  "Cal_Oct2017D_ref.root", "Cal_Oct2017C_ref.root", "Cal_Oct2017B_ref.root",
                                  "ultra_rereco2017.root"
                                 };



TGraphAsymmErrors* hist_ratio(TH2D *hist, Float_t R9boundary) {
	std::string name = "ratio_" + (std::string)hist->GetName();
	std::string title = hist->GetTitle();
	UInt_t boundaryBin = hist->GetYaxis()->FindBin(R9boundary);
	TH1D* lowR9 = hist->ProjectionX("lowR9", 1, boundaryBin - 1);
	TH1D* highR9 = hist->ProjectionX("highR9", boundaryBin, hist->GetYaxis()->GetNbins());
	TGraphAsymmErrors *ratioGraph = new TGraphAsymmErrors();
	ratioGraph->SetName(name.c_str());
	ratioGraph->SetTitle(title.c_str());
	ratioGraph->Divide(highR9, lowR9, "pois");
	ratioGraph->GetXaxis()->SetTimeDisplay(1);
	ratioGraph->GetXaxis()->SetTimeFormat("#splitline{%d-%b}{%H:%M}%F1970-01-01 00:00:00s0");
	ratioGraph->GetYaxis()->SetTitle("# of high R_{9} evts./# of low R_{9} evts.");
	lowR9->Delete();
	lowR9 = NULL;
	highR9->Delete();
	highR9 = NULL;
	return ratioGraph;
};

// std::vector<std::string> graphNames = {"R9eL_vs_time_in_EB", "R9eS_vs_time_in_EB", "R9eL_vs_time_in_EE", "R9eS_vs_time_in_EE"};
std::vector<std::string> graphNames = {"R9eL_vs_time_in_EE"};

void R9ratio(UInt_t i) {
	std::string file = files[i];
	std::string _filename = file;
	boost::replace_all(_filename, ".root", "");
	std::string writeDir;
	if (_filename.find("Cal") == std::string::npos)writeDir = "graphs_" + _filename + "/";
	else writeDir = filename + "/";

	TFile _file((jobdir + file).c_str(), "READ");

	for (auto & graph : graphNames) {
		TH2D* hist = (TH2D*) _file.Get(graph.c_str());
		TGraphAsymmErrors *R9ratio = hist_ratio(hist, 0.94);
		std::string outfilename = jobdir + writeDir + "graph_" + (std::string)R9ratio->GetName() + ".root";
		TFile writeFile(outfilename.c_str(), "RECREATE");
		writeFile.cd();
		R9ratio->Write();
		writeFile.Close();
	}
};